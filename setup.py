from setuptools import setup
 
package_name = 'live_plo'
 
setup(
    name=package_name,
    version='0.0.0',
    packages=[],
    py_modules=[
        'live_plotter',
    ],  
    install_requires=['setuptools'],
    zip_safe=True,
    author='user',
    author_email="user@todo.todo",
    maintainer='Calum Meiklejohn',
    maintainer_email="cmieklejohn@fmgl.com.au",
    keywords=['ROS', 'ROS2'],
    classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python',
        'Topic :: Software Development',
    ],  
    description='TODO: Package description.',
    license='Apache License, Version 2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'live_plotter = live_plotter:main',
        ],
    },  
)
