README.md

Starts a dash server on the machines ip address at port 8050.

Requires pandas, dash and numpy python libraries (install with pip).

Run with

ros2 run live_plotter live_plotter __params:=./src/live_plotter/config/params.yaml 

View the plot remotely.
    E.g if running locally go to
        http://127.0.0.1:8050/
    E.g for the AWL on zerotier VPN 
        http://172.16.0.87:8050/