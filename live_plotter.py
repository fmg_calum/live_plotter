#!/usr/bin/env python3
"""
Runs a dash server to plot ros2 topics live.

Requires numpy, dash and pandas python libraries.
"""
import rclpy
from rclpy.node import Node

import math
import random
import functools
import rclpy
from rclpy.node import Node
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import PointStamped
from geometry_msgs.msg import Point
from autonomy_msgs.msg import Pose
from visualization_msgs.msg import MarkerArray
from visualization_msgs.msg import Marker
from autonomy_msgs.msg import MobileObstacleArray
from autonomy_msgs.msg import MobileObstacle

from autonomy_msgs.msg import TelemetryData
from autonomy_msgs.msg import TranslationData
from autonomy_msgs.msg import Timestamp

import numpy
from rclpy.qos import qos_profile_default, qos_profile_sensor_data
from rclpy.qos import QoSProfile
from rclpy.qos import QoSReliabilityPolicy
from rclpy.qos import QoSDurabilityPolicy

import sys
from threading import Thread

import dash
from dash.dependencies import Output, Input
import dash_core_components as dcc
import dash_html_components as html
import plotly
import random
import plotly.graph_objs as go
from collections import deque

import plotly.express as px
import pandas as pd
import numpy as np

X = deque(maxlen=200)
X.append(1)
Y = deque(maxlen=200)
Y.append(1)

# dash server for plotting
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app = dash.Dash(__name__)
app.layout = html.Div(
    [
        dcc.Graph(id='live-graph', animate=True),
        dcc.Interval(
            id='graph-update',
            interval=1000,
            n_intervals = 0
        ),
    ]
)

plot_topic_values = []
latest_received_msg_timestamp = None

class LivePlotter(Node):                 # pylint: disable=too-many-instance-attributes
    """
    Creates live plot using plotly dash library to plot data from ros2 topics.
    """
    def __init__(self):
        super().__init__('close_object_simulator')
        result = self.declare_parameters(
            namespace='',
            parameters=[
                ('plot_topic', None),
                ('plot_update_hz', None)
                ]
        )

        plot_sub_name = "/translation_command"
        
        #import ipdb; ipdb.set_trace()
        plot_best_eff_qos_profile = QoSProfile(
            depth=1,
            reliability=QoSReliabilityPolicy.RMW_QOS_POLICY_RELIABILITY_BEST_EFFORT,
            durability=QoSDurabilityPolicy.RMW_QOS_POLICY_DURABILITY_VOLATILE
        )
        
        self.plot_sub_callback = self.create_subscription(TranslationData, 
                                    plot_sub_name, 
                                    self.plot_callback, 
                                    plot_best_eff_qos_profile)

  
        plot_topic_values = []

        # timer based publisher
        plot_hz = self.get_parameter('plot_update_hz').value
        if plot_hz > 0.0:
            self.timer_period = 1.0 / float(plot_hz)         # seconds
        else:
            self.timer_period = 0.1         # seconds
        #self.timer = self.create_timer(self.timer_period, self.timer_callback)

        
        self.latest_received_msg_timestamp = None

    def plot_callback(self, msg):
        """
        Adds data to be plotted
        """
        
        latest_received_msg_timestamp = float(msg.timestamp.seconds + 1e-9*msg.timestamp.nanos)

        X.append(latest_received_msg_timestamp)

        Y.append(msg.engine_demand.value)

    @app.callback(Output('live-graph', 'figure'),
        [Input('graph-update', 'n_intervals')])

    def dash_timer_callback(self):

        data = plotly.graph_objs.Scatter(
                x=list(X),
                y=list(Y),
                name='Scatter',
                mode= 'lines+markers'
                )

        return {'data': [data],'layout' : go.Layout(xaxis=dict(range=[min(X),max(X)]),
                                                    yaxis=dict(range=[min(Y),max(Y)]),)}


def main(args=None):
    rclpy.init(args=args)

    live_plotter = LivePlotter()
    #rclpy.spin(live_plotter)

    spin_thread = Thread(target=rclpy.spin, args=(live_plotter,))
    spin_thread.start()


    #app.run_server(debug=True)
    app.run_server(host= '0.0.0.0',debug=False)

    live_plotter.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()